package updaters

//go:generate charlatan -package mocks -output ../mocks/updater.go Updater

import (
	"continuous-evolution/src/project"
	"os"
	"path/filepath"
	"sync"

	"strings"

	"github.com/sirupsen/logrus"
)

//Config is configuration required by updater
type Config struct {
	PoolSize      int
	Parallel      int
	Docker        DockerConfig
	Dockercompose DockerComposeConfig
	Golangdep     GolangDepConfig
	Gradle        GradleConfig
	Maven         MavenConfig
	Npm           NpmConfig
	Pip           PipConfig
	Sbt           SbtConfig
}

//DefaultConfig is the default configuration required by updater
var DefaultConfig = Config{PoolSize: 1, Parallel: 1, Docker: DefaultDockerConfig, Dockercompose: DefaultDockerComposeConfig, Golangdep: DefaultGolangDepConfig,
	Gradle: DefaultGradleConfig, Maven: DefaultMavenConfig, Npm: DefaultNpmConfig, Pip: DefaultPipConfig, Sbt: DefaultSbtConfig}

var managerLogger = logrus.WithField("logger", "updaters/manager")

//Updater detect file can be updated and update it
type Updater interface {
	Update(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error)
}

type internalUpdater struct {
	IsPackageFile func(pathString string, f os.FileInfo) bool
	Updater       func(Config) Updater
}

type manager struct {
	config   Config
	updaters map[project.PackageType]internalUpdater
}

//Manager is reposible to download the code
type Manager interface {
	PackageFromFile(project.Option) project.Option
	Update(project.Option) project.Option
}

//NewManager return a Manager configured
func NewManager(config Config) Manager {
	updaters := make(map[project.PackageType]internalUpdater)
	if config.Npm.Enabled {
		updaters[project.PackageType("npm")] = internalUpdater{IsPackageFile: npmIsPackageFile, Updater: newNpm}
	}
	if config.Maven.Enabled {
		updaters[project.PackageType("mvn")] = internalUpdater{IsPackageFile: mvnIsPackageFile, Updater: newMvn}
	}
	if config.Pip.Enabled {
		updaters[project.PackageType("pip")] = internalUpdater{IsPackageFile: pipIsPackageFile, Updater: newPip}
	}
	if config.Docker.Enabled {
		updaters[project.PackageType("docker")] = internalUpdater{IsPackageFile: dockerIsPackageFile, Updater: newDocker}
	}
	if config.Dockercompose.Enabled {
		updaters[project.PackageType("docker-compose")] = internalUpdater{IsPackageFile: dockerComposeIsPackageFile, Updater: newDockerCompose}
	}
	if config.Golangdep.Enabled {
		updaters[project.PackageType("golangDep")] = internalUpdater{IsPackageFile: golangDepIsPackageFile, Updater: newGolangDep}
	}
	if config.Gradle.Enabled {
		updaters[project.PackageType("gradle")] = internalUpdater{IsPackageFile: gradleIsPackageFile, Updater: newGradle}
	}
	if config.Sbt.Enabled {
		updaters[project.PackageType("sbt")] = internalUpdater{IsPackageFile: sbtIsPackageFile, Updater: newSbt}
	}

	return &manager{
		config:   config,
		updaters: updaters,
	}
}

//PackageFromFile visit all project.Project files to find package to update. Add them to a new project.project and return it.
func (m *manager) PackageFromFile(o project.Option) project.Option {
	return o.ExecIfNoError(func(p project.Project) (project.Project, error) {
		packages := make([]project.Package, 0)

		lastNoGitDirs := "/impossibleDir" //Work because filepath.Walk iterates over dir
		visit := func(pathString string, f os.FileInfo, err error) error {
			if err != nil {
				managerLogger.WithField("path", pathString).WithError(err).Error("Can't visit file")
				return nil //return nil to continue visit
			}
			if !strings.HasPrefix(pathString, lastNoGitDirs) {
				isGitTracked, err := p.Git().TrackFile(pathString)
				if err != nil {
					managerLogger.WithField("path", pathString).WithError(err).Error("Can't get if file is git tracked")
					return nil //return nil to continue visit
				}
				if !isGitTracked && f.IsDir() {
					lastNoGitDirs = pathString + "/"
				}
				for updaterType, updater := range m.updaters {
					if isGitTracked && updater.IsPackageFile(pathString, f) {
						packages = append(packages, project.Package{Type: updaterType, Path: pathString})
					}
				}
			}
			return nil //return nil to continue visit
		}

		if err := filepath.Walk(p.AbsoluteDirectoy(), visit); err != nil {
			return p, err
		}

		managerLogger.WithField("project", p.Fullname()).WithField("pkg", packages).WithField("path", p.AbsoluteDirectoy()).Info("new packages found")
		return p.SetPackages(packages), nil
	})
}

//Update call all updater with project if project.Project.Packages.Type match updater.Type
func (m *manager) Update(o project.Option) project.Option {
	return o.ExecIfNoError(func(p project.Project) (project.Project, error) {
		packages := make([]project.Package, 0)

		managerLogger.Info("Start update project")
		muCopy := sync.Mutex{}
		waitUpdaters := sync.WaitGroup{}
		nbParallel := 0

		for updaterType, updater := range m.updaters {
			updaterInstance := updater.Updater(m.config)
			for _, pkg := range p.Packages() {
				if pkg.Type == updaterType {
					isExcludePkg := p.IsExcludesPkg(pkg)
					managerLogger.WithField("updater", pkg.Type).WithField("excludes", isExcludePkg).Info("Start update")
					if isExcludePkg {
						muCopy.Lock()
						packages = append(packages, pkg)
						muCopy.Unlock()
						continue //we exclude the file if no depth
					} else {
						excludes := p.HasExcludes(pkg)
						waitUpdaters.Add(1)
						nbParallel++
						go func(p project.Project, pkg project.Package, excludes []string) {
							newDepth, err := updaterInstance.Update(p.AbsoluteDirectoy(), pkg, excludes)
							if err != nil {
								managerLogger.WithField("pkg", pkg).WithError(err).Error("Error during update")
							} else {
								newPkg := pkg.AddUpdatedDependencies(newDepth)
								muCopy.Lock()
								packages = append(packages, newPkg)
								muCopy.Unlock()
							}
							waitUpdaters.Done()
						}(p, pkg, excludes)
					}
				}
				if nbParallel == m.config.Parallel {
					waitUpdaters.Wait()
					nbParallel = 0
					waitUpdaters = sync.WaitGroup{}
				}
			}
		}

		waitUpdaters.Wait()
		return p.SetPackages(packages), nil
	})
}
