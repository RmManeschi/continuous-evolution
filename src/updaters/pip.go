package updaters

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"os"
	"path"
)

//PipConfig is configuration required by pip updater
type PipConfig struct {
	Enabled bool
	Image   string
	Command []string
	Volumes []string
	Env     []string
	DNS     []string
}

//DefaultPipConfig is the default configuration required by pip updater
var DefaultPipConfig = PipConfig{
	Enabled: true,
	Image:   "chauffer/pip3-compile",
	Command: []string{"pip-compile", "--upgrade"},
	Volumes: []string{},
	Env:     []string{},
	DNS:     []string{},
}

func pipIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "requirements.txt"
}

type pipUpdater struct {
	config       PipConfig
	dockerClient tools.Docker
}

func newPip(config Config) Updater {
	return &pipUpdater{config: config.Pip, dockerClient: tools.NewDocker()}
}

func (u *pipUpdater) Update(_ string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	dirpath := path.Dir(pkg.Path)
	binds := append(u.config.Volumes, dirpath+":/tmp")
	u.dockerClient.StartDocker(u.config.Image, u.config.Command, binds, "/tmp", u.config.Env, u.config.DNS)
	return nil, nil
}
