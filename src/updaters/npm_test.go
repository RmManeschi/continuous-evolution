package updaters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"strings"
	"testing"
)

func TestNpmIsPackageFile(t *testing.T) {
	res := npmIsPackageFile("package.json", fakeFileInfo{name: "package.json", isDir: false})
	if !res {
		t.Fatal("npm updater should return true for package.json")
	}
	res = npmIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("npm updater should return false for other")
	}
	res = npmIsPackageFile("package.json", fakeFileInfo{name: "package.json", isDir: true})
	if res {
		t.Fatal("npm updater should return false for directory even if its name is package.json")
	}
}

func TestNpmUpdate(t *testing.T) {
	npm := newNpm(DefaultConfig)
	n := npm.(*npmUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	n.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		return []byte(`bad sentence
{"pkgToUpdate":"1.0.0"}
bad sentence`)
	}
	dep, err := n.Update("/tmp/npm", project.Package{Path: "/tmp/npm/package.json", Type: project.PackageType("npm"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err != nil {
		t.Fatal("Good conf should not throw error", err)
	}
	if len(dep) != 1 {
		t.Fatalf("npm should update 1 dep instead of %d", len(dep))
	}
}

func TestNpmUpdateEmpty(t *testing.T) {
	npm := newNpm(DefaultConfig)
	n := npm.(*npmUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	n.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		return []byte(``)
	}
	dep, err := n.Update("/tmp/npm", project.Package{Path: "/tmp/npm/package.json", Type: project.PackageType("npm"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err != nil {
		t.Fatal("No dep to update should not return error")
	}
	if len(dep) != 0 {
		t.Fatalf("No update available should not return dep to update but %d are returned", len(dep))
	}
}

func TestNpmUpdateError(t *testing.T) {
	npm := newNpm(DefaultConfig)
	n := npm.(*npmUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	n.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		return []byte(`bad sentence
{"pkgToUpdate
bad sentence`)
	}
	dep, err := n.Update("/tmp/npm", project.Package{Path: "/tmp/npm/package.json", Type: project.PackageType("npm"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err == nil {
		t.Fatal("Bad json formatting should throw err")
	}
	if len(dep) != 0 {
		t.Fatalf("error should not return dependencies to update but %d depencies are returned", len(dep))
	}
}

func TestNpmUpdateExclude(t *testing.T) {
	npm := newNpm(DefaultConfig)
	n := npm.(*npmUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	n.dockerClient = fakeDocker
	nbCallDocker := 0
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		if nbCallDocker == 0 && strings.Join(cmd, " ") != "--upgrade --upgradeAll --jsonUpgraded --packageFile /app/package.json --reject pkgToUpdate" {
			t.Fatalf("Bad npm cmd %s", strings.Join(cmd, " "))
		} else if nbCallDocker > 0 && strings.Join(cmd, " ") != "--upgrade --upgradeAll --packageFile /app/package.json --reject pkgToUpdate" {
			t.Fatalf("Bad npm cmd %s", strings.Join(cmd, " "))
		}
		nbCallDocker++
		return []byte(`bad sentence
{"pkgToUpdate2":"1.0.0"}
bad sentence`)
	}
	dep, err := n.Update("/tmp/npm", project.Package{Path: "/tmp/npm/package.json", Type: project.PackageType("npm"), UpdatedDependencies: []project.Dependency{}}, []string{"pkgToUpdate"})
	if err != nil {
		t.Fatal("Good conf should not throw error", err)
	}
	if len(dep) != 1 {
		t.Fatalf("npm should update 1 dep instead of %d", len(dep))
	}
}
