package updaters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"errors"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestNewManager(t *testing.T) {
	m := NewManager(DefaultConfig)
	if len(m.(*manager).updaters) != 8 {
		t.Fatal("By default all manager should be active")
	}
	m = NewManager(Config{Docker: DockerConfig{Enabled: true}})
	if len(m.(*manager).updaters) != 1 {
		t.Fatalf("only docker should be active instead of %d updaters", len(m.(*manager).updaters))
	}
}

func TestPackageFromFile(t *testing.T) {
	m := NewManager(Config{})
	err := os.MkdirAll("/tmp/updaters/testManager/dir1/subdir", 0777)
	if err != nil {
		t.Fatal("Can't write in tmp ?", err)
	}
	err = os.MkdirAll("/tmp/updaters/testManager/.gitignore/subdir", 0777)
	if err != nil {
		t.Fatal("Can't write in tmp ?", err)
	}
	err = os.MkdirAll("/tmp/updaters/testManager/errorGit/subdir", 0777)
	if err != nil {
		t.Fatal("Can't write in tmp ?", err)
	}
	err = ioutil.WriteFile("/tmp/updaters/testManager/dir1/subdir/fileToUpdate.json", []byte(""), 0777)
	if err != nil {
		t.Fatal("Can't write file ?", err)
	}
	fakeIsPackageFile := func(pathString string, f os.FileInfo) bool {
		if strings.HasPrefix(pathString, "/tmp/updaters/testManager/.gitignore") {
			t.Fatal("Manager should not acall updater with gitignored files")
		}
		return !f.IsDir() && f.Name() == "fileToUpdate.json"
	}
	fakeUpdater := func(Config) Updater {
		return mocks.NewFakeUpdaterDefaultFatal(t)
	}
	m.(*manager).updaters[project.PackageType("fake")] = internalUpdater{IsPackageFile: fakeIsPackageFile, Updater: fakeUpdater}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.AbsoluteDirectoyHook = func() string {
		return "/tmp/updaters"
	}
	fakeProject.FullnameHook = func() string {
		return "fullname"
	}
	fakeGit := mocks.NewFakeGitDefaultFatal(t)
	fakeGit.TrackFileHook = func(path string) (bool, error) {
		if path == "/tmp/updaters/testManager/errorGit" {
			return false, errors.New("Fake git error")
		}
		return path != "/tmp/updaters/testManager/.gitignore", nil
	}
	fakeProject.GitHook = func() project.Git {
		return fakeGit
	}
	fakeProject.SetPackagesHook = func(p []project.Package) project.Project {
		if len(p) != 1 {
			t.Fatalf("Should set only one package instead of %d", len(p))
		}
		if p[0].Type != project.PackageType("fake") {
			t.Fatalf("Should set good package type instead of %v", p[0].Type)
		}
		if p[0].Path != "/tmp/updaters/testManager/dir1/subdir/fileToUpdate.json" {
			t.Fatalf("Should set good path instead of %s", p[0].Path)
		}
		return fakeProject
	}
	m.PackageFromFile(project.NewOptional(fakeProject))
	fakeProject.AssertSetPackagesCalledOnce(t)
}

func TestUpdater(t *testing.T) {
	m := NewManager(Config{Parallel: 1})
	fakeIsPackageFile := func(pathString string, f os.FileInfo) bool {
		t.Fatal("Update should not call isPackageFile")
		return false
	}
	fakeUpdater := func(Config) Updater {
		u := mocks.NewFakeUpdaterDefaultFatal(t)
		u.UpdateHook = func(projectPath string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
			return []project.Dependency{{Name: "test", OriginalVersion: "1.0", NewVersion: "2.0"}}, nil
		}
		return u
	}
	m.(*manager).updaters[project.PackageType("fake")] = internalUpdater{IsPackageFile: fakeIsPackageFile, Updater: fakeUpdater}
	fakeProject := mocks.NewFakeProjectDefaultFatal(t)
	fakeProject.AbsoluteDirectoyHook = func() string {
		return "/"
	}
	fakeProject.PackagesHook = func() []project.Package {
		return []project.Package{{Path: "/", Type: project.PackageType("fake")}}
	}
	fakeProject.IsExcludesPkgHook = func(project.Package) bool {
		return false
	}
	fakeProject.HasExcludesHook = func(project.Package) []string {
		return []string{}
	}
	fakeProject.SetPackagesHook = func(pkg []project.Package) project.Project {
		if len(pkg) != 1 {
			t.Fatalf("Should update only one pkg instead of %d", len(pkg))
		}
		if len(pkg[0].UpdatedDependencies) != 1 {
			t.Fatalf("Should update only one dep instead of %d", len(pkg[0].UpdatedDependencies))
		}
		return fakeProject
	}
	p := m.Update(project.NewOptional(fakeProject))
	if p.IsError() {
		t.Fatal("Good conf should not throw error", p.Err())
	}
	if p.Get() == nil {
		t.Fatal("Update should not return nil project")
	}
	fakeProject.AssertSetPackagesCalledOnce(t)
	fakeProject.AssertIsExcludesPkgCalledOnce(t)
	fakeProject.AssertHasExcludesCalledOnce(t)
}
