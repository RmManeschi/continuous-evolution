package updaters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"strings"
	"testing"
)

func TestMvnIsPackageFile(t *testing.T) {
	res := mvnIsPackageFile("pom.xml", fakeFileInfo{name: "pom.xml", isDir: false})
	if !res {
		t.Fatal("mvn updater should return true for pom.xml")
	}
	res = mvnIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("mvn updater should return false for other")
	}
	res = mvnIsPackageFile("pom.xml", fakeFileInfo{name: "pom.xml", isDir: true})
	if res {
		t.Fatal("mvn updater should return false for directory even if its name is pom.xml")
	}
}

func TestMvnUpdateRoot(t *testing.T) {
	maven := newMvn(DefaultConfig)
	m := maven.(*mavenUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	m.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		if strings.Join(cmd, " ") != "mvn versions:use-latest-versions -DgenerateBackupPoms=false --non-recursive" {
			t.Fatalf("Bad maven cmd %s", strings.Join(cmd, " "))
		}
		return []byte(``)
	}
	_, err := m.Update("/tmp/maven", project.Package{Path: "/tmp/maven/pom.xml", Type: project.PackageType("mvn"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err != nil {
		t.Fatal("Good conf should not throw error")
	}
}

func TestMvnUpdateModule(t *testing.T) {
	maven := newMvn(DefaultConfig)
	m := maven.(*mavenUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	m.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		if strings.Join(cmd, " ") != "mvn versions:use-latest-versions -DgenerateBackupPoms=false" {
			t.Fatalf("Bad maven cmd %s", strings.Join(cmd, " "))
		}
		if workDir != "/tmp/module" {
			t.Fatalf("Workdir should be directory of module instad of %s", workDir)
		}
		return []byte(``)
	}
	_, err := m.Update("/tmp/maven", project.Package{Path: "/tmp/maven/module/pom.xml", Type: project.PackageType("mvn"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err != nil {
		t.Fatal("Good conf should not throw error")
	}
}

func TestMvnUpdateExclude(t *testing.T) {
	maven := newMvn(DefaultConfig)
	m := maven.(*mavenUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	m.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		if strings.Join(cmd, " ") != "mvn versions:use-latest-versions -DgenerateBackupPoms=false --non-recursive -Dexcludes=exclude" {
			t.Fatalf("Bad maven cmd %s", strings.Join(cmd, " "))
		}
		return []byte(``)
	}
	_, err := m.Update("/tmp/maven", project.Package{Path: "/tmp/maven/pom.xml", Type: project.PackageType("mvn"), UpdatedDependencies: []project.Dependency{}}, []string{"exclude"})
	if err != nil {
		t.Fatal("Good conf should not throw error")
	}
}
