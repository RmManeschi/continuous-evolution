package updaters

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"testing"
)

func TestPipIsPackageFile(t *testing.T) {
	res := pipIsPackageFile("requirements.txt", fakeFileInfo{name: "requirements.txt", isDir: false})
	if !res {
		t.Fatal("pip updater should return true for requirements.txt")
	}
	res = pipIsPackageFile("other", fakeFileInfo{name: "other", isDir: false})
	if res {
		t.Fatal("pip updater should return false for other")
	}
	res = pipIsPackageFile("requirements.txt", fakeFileInfo{name: "requirements.txt", isDir: true})
	if res {
		t.Fatal("pip updater should return false for directory even if its name is requirements.txt")
	}
}

func TestPipUpdate(t *testing.T) {
	pip := newPip(DefaultConfig)
	p := pip.(*pipUpdater)
	fakeDocker := mocks.NewFakeDockerDefaultFatal(t)
	p.dockerClient = fakeDocker
	fakeDocker.StartDockerHook = func(image string, cmd []string, binds []string, workDir string, env []string, dns []string) []byte {
		return []byte(``)
	}
	_, err := p.Update("/tmp/pip", project.Package{Path: "/tmp/pip/requirements.txt", Type: project.PackageType("pip"), UpdatedDependencies: []project.Dependency{}}, []string{})
	if err != nil {
		t.Fatal("Good conf should not throw error", err)
	}
}
