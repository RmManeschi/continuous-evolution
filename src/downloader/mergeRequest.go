package downloader

import (
	"continuous-evolution/src/project"
	"errors"
	"fmt"
	"net/url"
	"regexp"

	"github.com/sirupsen/logrus"
)

//MergeRequestConfig is configuration required by merge-request downloader
type MergeRequestConfig struct {
	Enabled bool
	URL     []string
}

//DefaultMergeRequestConfig is the default configuration required by merge-request downloader
var DefaultMergeRequestConfig = MergeRequestConfig{Enabled: true, URL: []string{"gitlab.com"}}

var loggerMergeRequest = logrus.WithField("logger", "downloader/mergeRequest")
var regexpMergeRequest = regexp.MustCompile(`https://([^/]+)/api/v4/projects/([^/]+)/([^/]+)/merge_requests/(.+)`)

type mergeRequest struct {
	config        Config
	projectFabric func(project.Input) project.Project
	retriever     RetrieveProject
	deleter       DeleteProject
}

func newMergeRequest(config Config, retriever RetrieveProject, deleter DeleteProject) downloader {
	return mergeRequest{config: config, projectFabric: project.New, retriever: retriever, deleter: deleter}
}

func (p mergeRequest) accept(url string) bool {
	res := regexpMergeRequest.FindStringSubmatch(url)
	return len(res) == 5 && p.acceptHost(res[1])
}

func (p mergeRequest) acceptHost(host string) bool {
	for _, h := range p.config.Mergerequest.URL {
		if h == host {
			return true
		}
	}
	return false
}

func (p mergeRequest) buildProject(gitlabURL string) (project.Project, error) {
	result := regexpMergeRequest.FindStringSubmatch(gitlabURL)

	host := result[1]
	organisation := result[2]
	projectName := result[3]
	reprocessDistantID := result[4]

	loggerPullRequest.WithFields(logrus.Fields{
		"host":               host,
		"organisation":       organisation,
		"projectName":        projectName,
		"reprocessDistantID": reprocessDistantID,
	}).Info("New merge request")

	login, token, ok := p.retriever.Retrieve(host, organisation, projectName)
	if !ok {
		loggerMergeRequest.WithField("gitlabURL", gitlabURL).Error("Project not found")
		return nil, errors.New("Project not found")
	}
	gitURL := fmt.Sprintf("https://%s:%s@%s/%s/%s.git", login, token, host, organisation, projectName)
	project := p.projectFabric(project.Input{
		Name:         projectName,
		GitURL:       gitURL,
		PathToWrite:  p.config.PathToWrite,
		BranchName:   p.config.BranchName,
		Login:        login,
		Token:        token,
		Host:         host,
		TypeHost:     project.Gitlab,
		Organisation: organisation,
	}).SetReProcessDistantID(reprocessDistantID)

	urlStr := fmt.Sprintf("https://%s/api/v4/projects/%s/merge_requests/%s?private_token=%s", host, url.PathEscape(organisation+"/"+projectName), reprocessDistantID, token)
	objectRes := make(map[string]interface{})
	err := project.HTTP().Get(urlStr, &objectRes)
	if err != nil {
		loggerMergeRequest.WithError(err).Error("Can't get merge-request details")
		return project, err
	}

	if objectRes["state"].(string) == "closed" {
		err := p.deleter.Delete(project)
		if err != nil {
			loggerMergeRequest.WithError(err).Error("Can't delete project")
			return project, err
		}
		return project, errors.New("Project is deleted by merge-request closed")
	}
	if objectRes["state"].(string) != "opened" {
		project = project.SetReProcessDistantID("")
	}

	bodyDetails := objectRes["description"].(string)
	pkgsToExcludeByFile := parseBody(bodyDetails)
	return project.SetExcludes(pkgsToExcludeByFile), nil
}

func (p mergeRequest) download(project project.Project) (project.Project, error) {
	if err := project.Git().Clone(); err != nil {
		loggerMergeRequest.Error(err)
		return project, err
	}
	return project, nil
}
