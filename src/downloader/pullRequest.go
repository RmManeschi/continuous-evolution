package downloader

import (
	"continuous-evolution/src/project"
	"errors"
	"fmt"
	"regexp"

	"github.com/sirupsen/logrus"
)

//PullRequestConfig is configuration required by pull-request downloader
type PullRequestConfig struct {
	Enabled bool
	URL     []string
}

//DefaultPullRequestConfig is the default configuration required by pull-request downloader
var DefaultPullRequestConfig = PullRequestConfig{Enabled: true, URL: []string{"github.com"}}

var loggerPullRequest = logrus.WithField("logger", "downloader/pullRequest")
var regexpPullRequest = regexp.MustCompile(`https://api.([^/]+)/repos/([^/]+)/([^/]+)/pulls/(.+)`)

type pullRequest struct {
	config        Config
	projectFabric func(project.Input) project.Project
	retriever     RetrieveProject
	deleter       DeleteProject
}

func newPullRequest(config Config, retriever RetrieveProject, deleter DeleteProject) downloader {
	return pullRequest{config: config, projectFabric: project.New, retriever: retriever, deleter: deleter}
}

func (p pullRequest) accept(url string) bool {
	res := regexpPullRequest.FindStringSubmatch(url)
	return len(res) == 5 && p.acceptHost(res[1])
}

func (p pullRequest) acceptHost(host string) bool {
	for _, h := range p.config.Pullrequest.URL {
		if h == host {
			return true
		}
	}
	return false
}

func (p pullRequest) buildProject(githubURL string) (project.Project, error) {
	result := regexpPullRequest.FindStringSubmatch(githubURL)

	host := result[1]
	organisation := result[2]
	projectName := result[3]
	reprocessDistantID := result[4]

	loggerPullRequest.WithFields(logrus.Fields{
		"host":               host,
		"organisation":       organisation,
		"projectName":        projectName,
		"reprocessDistantID": reprocessDistantID,
	}).Info("New pull request")

	login, token, ok := p.retriever.Retrieve(host, organisation, projectName)
	if !ok {
		loggerPullRequest.WithField("githubURL", githubURL).Error("Project not found")
		return nil, errors.New("Project not found")
	}
	gitURL := fmt.Sprintf("https://%s:%s@%s/%s/%s.git", login, token, host, organisation, projectName)
	project := p.projectFabric(project.Input{
		Name:         projectName,
		GitURL:       gitURL,
		PathToWrite:  p.config.PathToWrite,
		BranchName:   p.config.BranchName,
		Login:        login,
		Token:        token,
		Host:         host,
		TypeHost:     project.Github,
		Organisation: organisation,
	}).SetReProcessDistantID(reprocessDistantID)

	urlStr := fmt.Sprintf("https://api.%s/repos/%s/%s/pulls/%s", host, organisation, projectName, reprocessDistantID)
	objectRes := make(map[string]interface{})
	err := project.HTTP().Get(urlStr, &objectRes)
	if err != nil {
		loggerPullRequest.WithError(err).Error("Can't get pull-request details")
		return project, err
	}

	if objectRes["state"].(string) == "close" {
		err := p.deleter.Delete(project)
		if err != nil {
			loggerPullRequest.WithError(err).Error("Can't delete project")
			return project, err
		}
		return project, errors.New("Project is deleted by pull-request closed")
	}
	if objectRes["state"].(string) != "open" {
		project = project.SetReProcessDistantID("")
	}

	bodyDetails := objectRes["body"].(string)
	pkgsToExcludeByFile := parseBody(bodyDetails)
	return project.SetExcludes(pkgsToExcludeByFile), nil
}

func (p pullRequest) download(project project.Project) (project.Project, error) {
	if err := project.Git().Clone(); err != nil {
		loggerPullRequest.Error(err)
		return project, err
	}

	return project, nil
}
