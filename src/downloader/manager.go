package downloader

//go:generate charlatan -package mocks -output ../mocks/retrieveProject.go RetrieveProject
//go:generate charlatan -package mocks -output ../mocks/deleteProject.go DeleteProject
//go:generate charlatan -package mocks -output ../mocks/downloaderManager.go Manager

import (
	"continuous-evolution/src/project"
	"errors"
)

type downloader interface {
	accept(string) bool
	buildProject(string) (project.Project, error)
	download(project.Project) (project.Project, error)
}

//Config is configuration required by Downloader
type Config struct {
	PoolSize     int
	PathToWrite  string
	BranchName   string
	Local        LocalConfig
	Git          GitConfig
	Pullrequest  PullRequestConfig
	Mergerequest MergeRequestConfig
}

//DefaultConfig is the default configuration required by downloader
var DefaultConfig = Config{PoolSize: 1, PathToWrite: "/tmp", BranchName: "ConEvolUpdateLibs", Local: DefaultLocalConfig, Git: DefaultGitConfig, Pullrequest: DefaultPullRequestConfig, Mergerequest: DefaultMergeRequestConfig}

type manager struct {
	downloaders []downloader
}

//Manager is reposible to download the code
type Manager interface {
	//BuildProject create a project with an url
	BuildProject(string) (project.Project, error)
	//Download really download the project (git clone), and configure it (branch already exist, MR in progress...)
	Download(project.Option) project.Option
}

//RetrieveProject must give the complete saved project (with token...)
type RetrieveProject interface {
	Retrieve(host string, organisation string, name string) (string, string, bool)
}

//DeleteProject must delete the project when called
type DeleteProject interface {
	Delete(project.Project) error
}

//NewManager return a Manager configured
func NewManager(config Config, retriever RetrieveProject, deleter DeleteProject) Manager {
	downloaders := make([]downloader, 0)
	if config.Pullrequest.Enabled {
		downloaders = append(downloaders, newPullRequest(config, retriever, deleter)) // newPullRequest() must be before newGit() because it's more precise
	}
	if config.Mergerequest.Enabled {
		downloaders = append(downloaders, newMergeRequest(config, retriever, deleter)) // newMergeRequest() must be before newGit() because it's more precise
	}
	if config.Git.Enabled {
		downloaders = append(downloaders, newGit(config))
	}
	if config.Local.Enabled {
		downloaders = append(downloaders, newLocal(config))
	}
	return &manager{
		downloaders: downloaders,
	}
}

//BuildProject return project if at least one downloader will accept this url
func (m *manager) BuildProject(url string) (project.Project, error) {
	for _, downloader := range m.downloaders {
		if downloader.accept(url) {
			return downloader.buildProject(url)
		}
	}
	return nil, errors.New("your git url is not valid")
}

//Download use downloaders to download code
func (m *manager) Download(p project.Option) project.Option {
	return p.ExecIfNoError(func(p project.Project) (project.Project, error) {
		for _, downloader := range m.downloaders {
			if downloader.accept(p.GitURL()) {
				return downloader.download(p)
			}
		}
		return p, errors.New("No downloader found for this project")
	})
}
