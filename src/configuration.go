package main

import (
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/reporters"
	"continuous-evolution/src/scheduler"
	"continuous-evolution/src/updaters"
	"errors"
	"io/ioutil"

	"github.com/imdario/mergo"
	toml "github.com/pelletier/go-toml"
	"github.com/sirupsen/logrus"
)

var loggerConfiguration = logrus.WithField("logger", "configuration")

//Config required by Continuous-evolution to work
type Config struct {
	Version      string
	Orchestrator orchestrator.Config
	Downloader   downloader.Config
	Updater      updaters.Config
	Reporter     reporters.Config
	Scheduler    scheduler.Config
}

//GetConfig load config from file if configPath is not empty else return default config
func GetConfig(configPath string) (Config, error) {
	conf := Config{
		Version:      "1.0.0",
		Orchestrator: orchestrator.DefaultConfig,
		Downloader:   downloader.DefaultConfig,
		Updater:      updaters.DefaultConfig,
		Reporter:     reporters.DefaultConfig,
		Scheduler:    scheduler.DefaultConfig,
	}

	if configPath == "" {
		return conf, nil
	}

	tree, err := toml.LoadFile(configPath)
	if err != nil {
		loggerConfiguration.WithField("path", configPath).WithError(err).Error("Can't load config")
		return conf, err
	}

	userConf := Config{}
	if err := tree.Unmarshal(&userConf); err != nil {
		loggerConfiguration.WithField("path", configPath).WithError(err).Error("Can't unmarshal config")
		return conf, err
	}

	if userConf.Version == "" {
		loggerConfiguration.WithField("path", configPath).Error("No Version in config file")
		return conf, errors.New("Your configuration has no version")
	}

	if userConf.Version != conf.Version {
		loggerConfiguration.WithField("path", configPath).WithField("version", userConf.Version).Error("Version not supported")
		return conf, errors.New("Your configuration version is not supported")
	}

	err = mergo.MergeWithOverwrite(&conf, userConf) //add empty fields in userConf from conf
	if err != nil {
		loggerConfiguration.WithField("path", configPath).WithError(err).Error("Can't merge user conf and defaultConf")
		return conf, err
	}

	return conf, nil
}

//WriteConf generate default configuration file at the path given
func WriteConf(path string) error {
	conf, err := GetConfig("")
	if err != nil {
		return err
	}
	data, err := toml.Marshal(conf)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path, data, 0644)
}
