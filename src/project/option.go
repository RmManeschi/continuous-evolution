package project

//Option is a project and an optional error
type Option struct {
	p   Project
	err error
}

//NewOptional permit to create an Option with a project
func NewOptional(p Project) Option {
	return Option{p: p, err: nil}
}

//WithError add an error in this option
func (o Option) WithError(err error) Option {
	return Option{p: o.p, err: err}
}

//IsError return true if this option contain an error
func (o Option) IsError() bool {
	return o.err != nil
}

//Get return the project of this option
func (o Option) Get() Project {
	return o.p
}

//Err return the err of this option, nil if o.IsError() is false
func (o Option) Err() error {
	return o.err
}

//Exec call callback function with internal project and error. ANd return new Option with args returned by callback.
func (o Option) Exec(callback func(Project, error) (Project, error)) Option {
	p, err := callback(o.Get(), o.Err())
	return NewOptional(p).WithError(err)
}

//ExecIfNoError call callback function with internal project. And return new Option with args returned by callback. If the current option has an error directly return this option.
func (o Option) ExecIfNoError(callback func(Project) (Project, error)) Option {
	if !o.IsError() {
		p, err := callback(o.Get())
		return NewOptional(p).WithError(err)
	}
	return o
}
