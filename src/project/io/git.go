package io

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/sirupsen/logrus"
)

const (
	commitUpdate = "New dependencies"
	commitReset  = "Reset from master"
)

var loggerGit = logrus.WithField("logger", "project/io/git")

//Git is the struct to exec cmd git with project
type Git struct {
	pathToWrite string
	name        string
	gitURL      string
	branchName  string
}

//NewGit return an instance of Git struct
func NewGit(pathToWrite string, name string, gitURL string, branchName string) *Git {
	return &Git{pathToWrite: pathToWrite, name: name, gitURL: gitURL, branchName: branchName}
}

//Clone do git clone --depth 1 and switch go to g.branchName
func (g *Git) Clone() error {
	if !PathExists(g.pathToWrite) {
		err := os.MkdirAll(g.pathToWrite, os.ModePerm)
		if err != nil {
			loggerGit.
				WithField("dir", g.pathToWrite).
				WithError(err).
				Error("Error making mkdir -p")
			return err
		}
	}

	cmd := exec.Command("git", "clone", "--depth", "1", g.gitURL)
	cmd.Dir = g.pathToWrite
	output, err := cmd.Output()
	if err != nil {
		loggerGit.
			WithField("output", string(output)).
			WithField("cmd", "git clone "+g.gitURL).
			WithField("dir", g.pathToWrite).
			WithError(err).
			Error("Error making git clone")
		return err
	}

	cmd = exec.Command("git", "checkout", "-b", g.branchName)
	cmd.Dir = g.pathToWrite + "/" + g.name
	output, err = cmd.Output()
	if err != nil {
		loggerGit.
			WithField("output", string(output)).
			WithField("cmd", "git checkout -b "+g.branchName).
			WithField("dir", g.pathToWrite+"/"+g.name).
			WithError(err).
			Error("Error making git checkout")
		return err
	}

	return nil
}

//DefaultBranch return the name of the branch where HEAD is
func (g *Git) DefaultBranch() (string, error) {
	cmd := exec.Command("git", "rev-parse", "--abbrev-ref", "origin/HEAD")
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	output, err := cmd.CombinedOutput()
	if err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't get default branch")
		return "", err
	}
	defaultBranch := strings.Replace(string(output), "\n", "", -1)
	names := strings.Split(defaultBranch, "/")
	if len(names) != 2 {
		return "", fmt.Errorf("default branch can't be determined, from %s", defaultBranch)
	}
	return names[1], nil
}

//Diff return true if git workdir is not clean e.g. a modif is not committed
func (g *Git) Diff() bool {
	cmd := exec.Command("git", "diff", "--exit-code")
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	err := cmd.Run()
	if exiterr, ok := err.(*exec.ExitError); ok {
		if status, ok := exiterr.Sys().(syscall.WaitStatus); ok {
			if status.ExitStatus() == 1 {
				return true
			}
		}
	}
	if err != nil {
		loggerGit.WithError(err).Error("git diff")
	}
	//exit code 0 == no modif OR > 1 == other errors
	return false
}

//Commit add current code in git
func (g *Git) Commit() error {
	cmd := exec.Command("git", "commit", "-am", commitUpdate)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't commit")
		return err
	}
	loggerGit.WithField("Path", cmd.Dir).Info("Commit")
	return nil
}

//PushForce push current modifications
func (g *Git) PushForce() error {
	cmd := exec.Command("git", "push", "-f", "origin", g.branchName)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)
	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't push")
		return err
	}
	loggerGit.WithField("Path", cmd.Dir).Info("Push -f")
	return nil
}

//TrackFile return true if its a file tracked by git, false otherwise
func (g *Git) TrackFile(path string) (bool, error) {
	cmd := exec.Command("git", "ls-files", path)
	cmd.Dir = fmt.Sprintf("%s/%s", g.pathToWrite, g.name)

	if output, err := cmd.CombinedOutput(); err != nil {
		loggerGit.WithField("output", string(output)).WithError(err).Error("Can't ls-files")
		return false, err
	} else if len(output) == 0 {
		return false, nil
	}
	return true, nil
}
