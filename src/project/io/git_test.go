package io

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
)

var loggerGitTest = logrus.WithField("logger", "project/io/git_test")

func start(cmd string, dir string) {
	split := strings.Split(cmd, " ")
	c := exec.Command(split[0], split[1:]...)
	c.Dir = dir
	output, err := c.CombinedOutput()
	if err != nil {
		loggerGitTest.WithField("cmd", cmd).WithField("path", dir).WithField("output", string(output)).WithError(err).Error("cmd error")
	} else {
		loggerGitTest.WithField("cmd", cmd).WithField("path", dir).WithField("output", string(output)).Info("cmd success")
	}
}

type gitServer struct {
	url           string
	path          string
	currentBranch string
	createBranch  bool
}

func newGitServer(name string) *gitServer {
	serverPath, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		panic(err)
	}
	start("git --bare init "+name, serverPath)
	return &gitServer{url: fmt.Sprintf("file://%s/%s", serverPath, name), path: serverPath, currentBranch: "master", createBranch: false}
}

func (gitServer *gitServer) Close() error {
	return os.RemoveAll(gitServer.path)
}

func (gitServer *gitServer) goToBranch(branch string) *gitServer {
	gitServer.currentBranch = branch
	return gitServer
}

func (gitServer *gitServer) createAndGoToBranch(branch string) *gitServer {
	gitServer.currentBranch = branch
	gitServer.createBranch = true
	return gitServer.goToBranch(branch)
}

func (gitServer *gitServer) add(path string, content string) *gitServer {
	name, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		panic(err)
	}
	start("git clone "+gitServer.url+" testGit", name)
	if !gitServer.createBranch {
		start("git checkout "+gitServer.currentBranch, name+"/testGit")
	} else {
		start("git checkout -b "+gitServer.currentBranch, name+"/testGit")
		gitServer.createBranch = false
	}
	ioutil.WriteFile(name+"/testGit/"+path, []byte(content), 0644)
	start("git add .", name+"/testGit")
	start("git commit -m \"update_"+path+"\"", name+"/testGit")
	start("git push origin "+gitServer.currentBranch, name+"/testGit")
	if err := os.RemoveAll(name); err != nil {
		panic(err)
	}
	return gitServer
}

func (gitServer *gitServer) rm(path string) *gitServer {
	name, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		panic(err)
	}
	start("git clone "+gitServer.url+" testGit", name)
	start("git checkout "+gitServer.currentBranch, name+"/testGit")
	start("git rm "+name+"/testGit/"+path, name+"/testGit")
	start("git commit -m \"delete"+strings.Replace(path, "/", "_", -1)+"\"", name+"/testGit")
	start("git push origin "+gitServer.currentBranch, name+"/testGit")
	if err := os.RemoveAll(name); err != nil {
		panic(err)
	}
	return gitServer
}

func checkCurrentBranch(path string) (string, error) {
	c := exec.Command("git", "branch")
	c.Dir = path
	output, err := c.Output()
	if err != nil {
		return "", err
	}
	return strings.TrimSpace(strings.Split(string(output), " ")[1]), err
}

func TestGitClone(t *testing.T) {
	gitServer := newGitServer("testGit").
		goToBranch("master").
		add("hello", "world")
	clientPath, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		t.Fatal("Can't create temporary path for client", err)
	}
	err = NewGit(clientPath, "testGit", gitServer.url, "ConEvolTest").Clone()
	if err != nil {
		t.Fatal("Error on git clone", err)
	}
	if !PathExists(clientPath + "/testGit/hello") {
		t.Fatal("Clone must create directory")
	}
	if branch, err := checkCurrentBranch(clientPath + "/testGit"); err != nil {
		t.Fatal("Can't get current branch", err)
	} else if branch != "ConEvolTest" {
		t.Fatalf("Current branch must be ConEvolTest instead of %s", branch)
	}
	if err := gitServer.Close(); err != nil {
		t.Fatal("Can't close git server", err)
	}
	if err := os.RemoveAll(clientPath); err != nil {
		t.Fatal("Can't delete temporary path for client", err)
	}
}

func TestGitCloneExistingBranchWithConflict(t *testing.T) {
	gitServer := newGitServer("testGit3").
		goToBranch("master").
		add("hello", "world").
		createAndGoToBranch("ConEvolTest").
		add("hello", "world2").
		goToBranch("master").
		add("hello", "world3").
		add("hello2", "world").
		add("hello3", "world")
	clientPath, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		t.Fatal("Can't create temporary path for client", err)
	}
	err = NewGit(clientPath, "testGit3", gitServer.url, "ConEvolTest").Clone()
	if err != nil {
		t.Fatal("Error on git clone", err)
	}
	if !PathExists(clientPath + "/testGit3/hello") {
		t.Fatal("Clone must create directory")
	}
	if !PathExists(clientPath + "/testGit3/hello2") {
		t.Fatal("Clone must create hello2")
	}
	if !PathExists(clientPath + "/testGit3/hello3") {
		t.Fatal("Clone must create hello3")
	}
	data, err := ioutil.ReadFile(clientPath + "/testGit3/hello")
	if err != nil {
		t.Fatal("Clone must have file", err)
	}
	if string(data) != "world3" {
		t.Fatalf("hello file must contain world3 instead of %s", string(data))
	}
	if branch, err := checkCurrentBranch(clientPath + "/testGit3"); err != nil {
		t.Fatal("Can't get current branch", err)
	} else if branch != "ConEvolTest" {
		t.Fatalf("Current branch must be ConEvolTest instead of %s", branch)
	}
	if err := gitServer.Close(); err != nil {
		t.Fatal("Can't close git server", err)
	}
	if err := os.RemoveAll(clientPath); err != nil {
		t.Fatal("Can't delete temporary path for client", err)
	}
}

func TestGitCloneExistingBranchDeleteInMaster(t *testing.T) {
	gitServer := newGitServer("testGit3").
		/**
		* 5a51bb5 - Reset from master - (ConEvolTest)
		* 96713ae - "update_tmp_testGit2_hello" - (origin/ConEvolTest)
		| * 46153e9 - "delete_tmp_testGit2_hello" - (origin/master, origin/HEAD, master)
		| * bce7da7 - "update_tmp_testGit2_hello2" -
		|/
		* 37a9e84 - "update_tmp_testGit2_hello" -
		*/
		goToBranch("master").
		add("hello", "world").
		createAndGoToBranch("ConEvolTest").
		add("hello", "world2").
		goToBranch("master").
		add("hello2", "world").
		rm("hello")
	clientPath, err := ioutil.TempDir("", "ConEvolTest")
	if err != nil {
		t.Fatal("Can't create temporary path for client", err)
	}
	err = NewGit(clientPath, "testGit3", gitServer.url, "ConEvolTest").Clone()
	if err != nil {
		t.Fatal("Error on git clone", err)
	}
	if !PathExists(clientPath + "/testGit3/hello2") {
		t.Fatal("Clone must create directory")
	}
	if PathExists(clientPath + "/testGit3/hello") {
		t.Fatal("hello is deleted in master and must be deleted in ConEvolTest branch")
	}
	if branch, err := checkCurrentBranch(clientPath + "/testGit3"); err != nil {
		t.Fatal("Can't get current branch", err)
	} else if branch != "ConEvolTest" {
		t.Fatalf("Current branch must be ConEvolTest instead of %s", branch)
	}
	if err := gitServer.Close(); err != nil {
		t.Fatal("Can't close git server", err)
	}
	if err := os.RemoveAll(clientPath); err != nil {
		t.Fatal("Can't delete temporary path for client", err)
	}
}
