package orchestrator

import (
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"testing"
	"time"
)

func TestNewGitlabError(t *testing.T) {
	_, err := newGitlab(DefaultConfig, nil)
	if err == nil {
		t.Fatal("default conf should throw err because no login")
	}

	_, err = newGitlab(Config{Gitlab: GitlabConfig{Enabled: true, DurationFetch: "3x", Instances: make([]GitlabInstance, 0)}}, nil)
	if err == nil {
		t.Fatal("conf with duration error should throw err")
	}
}

func TestGitlabWithoutProjectAlreadySave(t *testing.T) {
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(p project.Option) {}
	fakeSender.CloseHook = func() {}

	g, err := newGitlab(Config{Gitlab: GitlabConfig{Enabled: true, DurationFetch: "10ms", Instances: []GitlabInstance{{APIURL: "https://gitlab.com/api/v4", Login: "login", PrivateToken: "token"}}}}, fakeSender)
	if err != nil {
		t.Fatal("good conf should not throw err", err)
	}

	fakeProjectAlreadySave := mocks.NewFakeProjectAlreadySaveDefaultFatal(t)
	fakeProjectAlreadySave.IsEnabledHook = func() bool {
		return false
	}

	err = g.start(nil, fakeProjectAlreadySave)

	if err == nil {
		t.Fatal("If scheduler is not enabled gitlab should not start with err")
	}

	fakeProjectAlreadySave.AssertIsEnabledCalledOnce(t)
}

func TestGitlab(t *testing.T) {
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(p project.Option) {}
	fakeSender.CloseHook = func() {}

	g, err := newGitlab(Config{Gitlab: GitlabConfig{Enabled: true, DurationFetch: "10ms", Instances: []GitlabInstance{{APIURL: "https://gitlab.com/api/v4", Login: "login", PrivateToken: "token"}}}}, fakeSender)
	if err != nil {
		t.Fatal("good conf should not throw err", err)
	}

	fakeHTTP := mocks.NewFakeHTTPDefaultFatal(t)
	fakeHTTP.GetHook = func(url string, toReceive interface{}) error {
		data := ""
		if url == "https://gitlab.com/api/v4/projects?membership=true&with_merge_requests_enabled=true&private_token=token" {
			data = `[
				{"http_url_to_repo":"https://gitlab.com/orga/project.git", "permissions":{"project_access":{"access_level":30}}},
				{"http_url_to_repo":"https://gitlab.com/orga/project2.git", "permissions":{"project_access":{"access_level":20}}}
			]`
		} else {
			return errors.New("Get url not authorized " + url)
		}
		json.Unmarshal([]byte(data), toReceive)
		return nil
	}
	g.httpFactory = func(login string, token string) project.HTTP {
		return fakeHTTP
	}

	fakeDownloader := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloader.BuildProjectHook = func(p string) (project.Project, error) {
		if p != "https://login:token@gitlab.com/orga/project.git" {
			t.Fatal("Bad project git url", p)
			return nil, errors.New("Bad project git url")
		}
		fakeProject := mocks.NewFakeProjectDefaultFatal(t)
		fakeProject.FullnameHook = func() string {
			return "gitlab.com/orga/project"
		}
		return fakeProject, nil
	}

	fakeProjectAlreadySave := mocks.NewFakeProjectAlreadySaveDefaultFatal(t)
	fakeProjectAlreadySave.IsEnabledHook = func() bool {
		return true
	}
	nbCallAlreadySave := 0
	fakeProjectAlreadySave.ExistHook = func(p project.Project) bool {
		nbCallAlreadySave++
		return nbCallAlreadySave < 2
	}

	g.start(fakeDownloader, fakeProjectAlreadySave)

	time.Sleep(11 * time.Millisecond)
	g.close()
	fakeDownloader.AssertBuildProjectCalledN(t, 2)
	fakeSender.AssertSendCalledOnce(t)
	fakeSender.AssertCloseCalledOnce(t)
}
