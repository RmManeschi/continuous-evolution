package orchestrator

import (
	"context"
	"continuous-evolution/src/mocks"
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/shurcooL/githubv4"
)

func TestNewGithubError(t *testing.T) {
	_, err := newGithub(DefaultConfig, nil)
	if err == nil {
		t.Fatal("default conf should throw err because no login")
	}

	_, err = newGithub(Config{Github: GithubConfig{Enabled: true, DurationFetch: "3x", Instances: make([]GithubInstance, 0)}}, nil)
	if err == nil {
		t.Fatal("conf with duration error should throw err")
	}
}

func TestGithubWithoutProjectAlreadySave(t *testing.T) {
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(p project.Option) {}
	fakeSender.CloseHook = func() {}

	g, err := newGithub(Config{Github: GithubConfig{Enabled: true, DurationFetch: "10ms", Instances: []GithubInstance{{APIURL: "https://api.github.com", Login: "login", PrivateToken: "token"}}}}, fakeSender)
	if err != nil {
		t.Fatal("good conf should not throw err", err)
	}

	fakeProjectAlreadySave := mocks.NewFakeProjectAlreadySaveDefaultFatal(t)
	fakeProjectAlreadySave.IsEnabledHook = func() bool {
		return false
	}

	err = g.start(nil, fakeProjectAlreadySave)

	if err == nil {
		t.Fatal("If scheduler is not enabled github should not start with err")
	}

	fakeProjectAlreadySave.AssertIsEnabledCalledOnce(t)
}

//TODO add data test with :
//   * at least 2 round
//   * errors all levels
//   * 2 github instances with good url check

func TestGithubStart(t *testing.T) {
	fakeSender := mocks.NewFakeSenderDefaultFatal(t)
	fakeSender.SendHook = func(p project.Option) {}
	fakeSender.CloseHook = func() {}

	g, err := newGithub(Config{Github: GithubConfig{Enabled: true, DurationFetch: "10ms", Instances: []GithubInstance{{APIURL: "https://api.github.com", Login: "login", PrivateToken: "token"}}}}, fakeSender)
	if err != nil {
		t.Fatal("good conf should not throw err", err)
	}

	fakeHTTP := mocks.NewFakeHTTPDefaultFatal(t)
	fakeHTTP.GetHook = func(url string, toReceive interface{}) error {
		data := ""
		if url == "https://api.github.com/user/repository_invitations" {
			data = `[{"url":"repoInvite"}]`
		} else if url == "https://api.github.com/user/memberships/orgs?state=pending" {
			data = `[{"organization":{"login":"orgaInvite"}}]`
		} else {
			return errors.New("Get url not authorized " + url)
		}
		json.Unmarshal([]byte(data), toReceive)
		return nil
	}
	fakeHTTP.PatchHook = func(url string, toSend interface{}, toReceive interface{}) error {
		if url != "repoInvite" && url != "https://api.github.com/user/memberships/orgs/orgaInvite" {
			return errors.New("Patch url not authorized " + url)
		}
		return nil
	}
	g.httpFactory = func(login string, token string) project.HTTP {
		return fakeHTTP
	}

	fakeGraph := mocks.NewFakeGraphQlQueryDefaultFatal(t)
	fakeGraph.QueryHook = func(ctx context.Context, query interface{}, params map[string]interface{}) error {
		query.(*gqlGithubQuery).Viewer.Login = "login"
		u, _ := url.Parse("https://github.com/orga/project")
		r := repository{
			Name:  "project",
			URL:   &githubv4.URI{URL: u},
			Owner: ownerRepository{Login: "orga"},
		}
		query.(*gqlGithubQuery).Viewer.Repositories.Nodes = append(query.(*gqlGithubQuery).Viewer.Repositories.Nodes, r)
		return nil
	}
	g.graphQlFactory = func(url string, httpClient *http.Client) GraphQlQuery {
		return fakeGraph
	}

	fakeDownloader := mocks.NewFakeManagerDefaultFatal(t)
	fakeDownloader.BuildProjectHook = func(p string) (project.Project, error) {
		if p != "https://login:token@github.com/orga/project.git" {
			t.Fatal("Bad project git url", p)
			return nil, errors.New("Bad project git url")
		}
		return mocks.NewFakeProjectDefaultFatal(t), nil
	}
	fakeProjectAlreadySave := mocks.NewFakeProjectAlreadySaveDefaultFatal(t)
	fakeProjectAlreadySave.IsEnabledHook = func() bool {
		return true
	}
	nbCallAlreadySave := 0
	fakeProjectAlreadySave.ExistHook = func(p project.Project) bool {
		nbCallAlreadySave++
		return nbCallAlreadySave < 2
	}

	g.start(fakeDownloader, fakeProjectAlreadySave)

	time.Sleep(11 * time.Millisecond)
	g.close()
	fakeGraph.AssertQueryCalledN(t, 2)
	fakeDownloader.AssertBuildProjectCalledN(t, 2)
	fakeSender.AssertSendCalledOnce(t)
	fakeSender.AssertCloseCalledOnce(t)
}
