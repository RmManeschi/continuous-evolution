SHELL := /bin/bash
VERSION=`git describe --exact-match --tags HEAD 2> /dev/null`
COMMIT=`git rev-parse HEAD`
DATE_BUILD=`date +%Y-%m-%d\_%H:%M`
DEP_VERSION=v0.5.0

BIN_DIR := $(GOPATH)/bin
DEP := $(BIN_DIR)/dep
GOLINT := $(BIN_DIR)/golint
CHARLATAN := $(BIN_DIR)/charlatan
GOIMPORTS := $(BIN_DIR)/goimports

PACKAGES := $(go list ./src/... | grep -v mocks)

.PHONY: first
first: build

######################
## DEP
######################
$(DEP):
	curl -L -s https://github.com/golang/dep/releases/download/${DEP_VERSION}/dep-linux-amd64 -o $(BIN_DIR)/dep
	chmod +x $(BIN_DIR)/dep

vendor: $(DEP) ## Install dependencies
	dep ensure

$(GOLINT): vendor
	cd vendor/github.com/golang/lint/golint && go install .

$(CHARLATAN): vendor
	cd vendor/github.com/percolate/charlatan && go install .

$(GOIMPORTS): vendor
	cd vendor/golang.org/x/tools/cmd/goimports && go install .
######################
## LINT
######################
.PHONY: lint
lint: $(GOLINT) ## Start lint
	diff -u <(echo -n) <(gofmt -s -d ./src); [ $$? -eq 0 ]
	go tool vet -composites=false -shadow=true src/**/*.go
	diff -u <(echo -n) <(golint $(PACKAGES)); [ $$? -eq 0 ]

######################
## TEST
######################
.PHONY: test
test: vendor ## Play test with race flag
	go test -race ./src/...

.PHONY: test-verbose
test-verbose: vendor ## Play test with race flag
	go test -race -v ./src/...

.PHONY: cover
cover: vendor ## Display test coverage percent
	./cover.sh -terminal

.PHONY: mocks
mocks: $(CHARLATAN) $(GOIMPORTS) ## Generate mocks
	go generate ./src/...
	sed -i '6iimport . "continuous-evolution/src/project"' ./src/mocks/project.go
	goimports -w ./src/

######################
## BUILD
######################
.PHONY: build
build: vendor ## Build binary
	rm -rf build
	CGO_ENABLED=0 go build -ldflags "-extldflags '-static' -X main.version=${VERSION} -X main.commit=${COMMIT} -X main.date=${DATE_BUILD}" -o "build/cli" src/main.go src/configuration.go

######################
## DOC
######################
.PHONY: doc
doc: ## Make doc and put minified html into public directory
	./makeDoc.sh
	rm -rf doc/public
	mv doc/public_min public

.PHONY: doc-dev
doc-dev: ## Make doc, watch files and launch a light weight http server to access
	./makeDoc.sh -dev

######################
## CLEAN
######################
.PHONY: clean
clean: ## Remove vendors, compiled tools, previous build and doc temporary files
	rm -f $(DEP)
	rm -f $(GOLINT)
	rm -f $(CHARLATAN)
	rm -f $(GOIMPORTS)
	rm -rf vendor
	rm -rf build
	rm -rf doc/public
	rm -rf doc/public_min
	rm -rf doc/themes

####################
## HELP
#####
.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'